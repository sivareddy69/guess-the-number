let gameResult = document.getElementById("gameResult");
let userInput = document.getElementById("userInput");
let randomNumber = Math.ceil(Math.random() * 100);

function checkGuess() {
    let guessedNumber = parseInt(userInput.value);
    if (guessedNumber > randomNumber) {
        gameResult.textContent = "Too High try again";
        gameResult.style.backgroundColor = "orange";
    } else if (guessedNumber < randomNumber) {
        gameResult.textContent = "Too Low Try again";
        gameResult.style.backgroundColor = "orange";
    } else if (guessedNumber === randomNumber) {
        gameResult.textContent = "Congratulations, you got it";
        gameResult.style.backgroundColor = "green";
    } else {
        gameResult.textContent = "Please provide a valid Input";
        gameResult.style.backgroundColor = "red";
    }


}